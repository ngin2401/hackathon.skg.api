﻿using Hackathon.SKG.Api.Model;
using MongoDbGenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon.SKG.Api.Repository
{
    public interface IUsersRepository : IBaseMongoRepository
    {
        List<Users> GetAll();
        List<Users> GetByIds(List<Guid> ids);
    }
}
