﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hackathon.SKG.Api.Model.Settings;
using Hackathon.SKG.Api.Proxy;
using Hackathon.SKG.Api.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace Hackathon.SKG.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<MongoConnection>(options =>
            {
                options.ConnectionString
                    = Configuration.GetSection("MongoConnection:ConnectionString").Value;
                options.Database
                    = Configuration.GetSection("MongoConnection:Database").Value;
            });

            #region DI
            services.AddScoped<IUsersRepository, UsersRepository>();
            services.AddScoped<IAssignmentRepository, AssignmentRepository>();
            // services.AddScoped<ICoursesRepository, CoursesRepository>();
            services.AddScoped<IFileAssignmentRepository, FileAssignmentRepository>();
            services.AddScoped<IFirebaseProxy, FirebaseProxy>();

            
            #endregion


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScoped<ApiValidationFilterAttribute>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Hackathon.SKG.Api", Version = "v1" });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //app.UseStatusCodePagesWithReExecute("/error/{0}");
            //app.UseExceptionHandler("/error/500");
            app.UseErrorWrapping();

            
            app.UseMvc();

            app.UseSwagger(c =>
            {
                c.RouteTemplate =
                    "api-docs/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                //Include virtual directory if site is configured so
                c.RoutePrefix = "api-docs";
                c.SwaggerEndpoint("./v1/swagger.json", "Api v1");
            });
        }
    }
}
