﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FcmSharp.Requests;
using Hackathon.SKG.Api.Dto;
using Hackathon.SKG.Api.Model;
using Hackathon.SKG.Api.Proxy;
using Hackathon.SKG.Api.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Hackathon.SKG.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private IFirebaseProxy firebaseProxy;
        private IUsersRepository usersRepository;
        private IAssignmentRepository assignmentRepository;

        public NotificationController(IFirebaseProxy firebaseProxy
            , IUsersRepository usersRepository
            , IAssignmentRepository assignmentRepository
            )
        {
            this.firebaseProxy = firebaseProxy;
            this.usersRepository = usersRepository;
            this.assignmentRepository = assignmentRepository;
        }

        [Route("sendbykey")]
        [HttpPost]
        public ActionResult<ApiOkResponse<object>> SendByKey(SendNotificationRequest sendNotificationRequest)
        {
            var result = firebaseProxy.SendNotification(sendNotificationRequest);
            return Ok(new ApiOkResponse(result));
        }

        [Route("sendallstudentbyassignment")]
        [HttpPost]
        public ActionResult<ApiOkResponse<Notification>> SendAllStudentByAssignment(SendAllStudentByAssignment sendAllStudentByAssignment)
        {
            var assignment = assignmentRepository.GetById<Assignment>(sendAllStudentByAssignment.AssignmentId);
            if (assignment == null)
            {
                return BadRequest(new ApiBadRequestResponse($"Assignment {sendAllStudentByAssignment.AssignmentId} not found"));
            }
            Notification notification = new Notification() {
                Title = $"{assignment.Name} - {assignment.CourseName} mới được tạo",
                Body = $"{assignment.Name} - {assignment.CourseName} mới được tạo vào {assignment.StartDate.ToShortTimeString()} mời sinh viên vào kiểm tra",
            };
            var users = usersRepository.GetByIds(assignment.ListStudent);
            foreach (var item in users)
            {
                foreach (var token in item.NotiToken)
                {
                    var result = firebaseProxy.SendNotification(new SendNotificationRequest
                    {
                        ClientToken = token,
                        Notification = notification
                    });
                }
                

            }
            return Ok(new ApiOkResponse(notification));
        }

        [Route("warningstudentassignment")]
        [HttpPost]
        public ActionResult<ApiOkResponse<Notification>> WarningStudentAssignment(SendAllStudentByAssignment sendAllStudentByAssignment)
        {
            var assignment = assignmentRepository.GetById<Assignment>(sendAllStudentByAssignment.AssignmentId);
            if (assignment == null)
            {
                return BadRequest(new ApiBadRequestResponse($"Assignment {sendAllStudentByAssignment.AssignmentId} not found"));
            }
            Notification notification = new Notification()
            {
                Title = $"{assignment.Name} - {assignment.CourseName} sắp hết hạn",
                Body = $"{assignment.Name} - {assignment.CourseName}  sắp hết hạn vào {assignment.EndDate.ToShortTimeString()} mời sinh viên vào kiểm tra",
            };
            var users = usersRepository.GetByIds(assignment.ListStudent);
            foreach (var item in users)
            {
                foreach (var token in item.NotiToken)
                {
                    var result = firebaseProxy.SendNotification(new SendNotificationRequest
                    {
                        ClientToken = token,
                        Notification = notification
                    });
                }


            }
            return Ok(new ApiOkResponse(notification));
        }
    }
}
