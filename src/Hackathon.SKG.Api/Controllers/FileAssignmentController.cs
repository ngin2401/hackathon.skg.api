﻿using Firebase.Storage;
using Hackathon.SKG.Api.Model;
using Hackathon.SKG.Api.Repository;
using Hackathon.SKG.Api.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hackathon.SKG.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class FileAssignmentController : ControllerBase
    {
        private IAssignmentRepository assignmentRepository;

        private IFileAssignmentRepository fileAssignmentRepository;

        public FileAssignmentController(IAssignmentRepository assignmentRepository
            , IFileAssignmentRepository fileAssignmentRepository
            )
        {
            this.assignmentRepository = assignmentRepository;
            this.fileAssignmentRepository = fileAssignmentRepository;
        }

        /// <summary>
        /// để null get all, truyền id get id nếu không lấy name
        /// </summary>
        /// <param name="searchFileAssignmentRequest">để null get all, truyền id get id nếu không lấy name</param>
        /// <returns></returns>
        // GET api/values
        [HttpGet]
        public ActionResult<ApiOkResponse<List<FileAssignment>>> Get([FromQuery] SearchFileAssignmentRequest searchFileAssignmentRequest = null)
        {
            if (searchFileAssignmentRequest.Id.HasValue)
            {
                return Ok(new ApiOkResponse(assignmentRepository.GetById<FileAssignment>(searchFileAssignmentRequest.Id.Value)));
            }

            if (searchFileAssignmentRequest.StudentId.HasValue
                && searchFileAssignmentRequest.AssignmentId.HasValue
                )
            {
                return Ok(new ApiOkResponse(assignmentRepository
                    .GetAll<FileAssignment>(x =>
                    x.StudentId == searchFileAssignmentRequest.StudentId
                    && x.AssignmentId == searchFileAssignmentRequest.AssignmentId
                    )));
            }

            if (searchFileAssignmentRequest.AssignmentId.HasValue
               && searchFileAssignmentRequest.Status.HasValue
               )
            {
                return Ok(new ApiOkResponse(assignmentRepository
                    .GetAll<FileAssignment>(x =>
                    x.Status == searchFileAssignmentRequest.Status
                    && x.AssignmentId == searchFileAssignmentRequest.AssignmentId
                    )));
            }

            //if (searchFileAssignmentRequest.CourseId.HasValue)
            //{
            //    return Ok(new ApiOkResponse(assignmentRepository
            //        .GetAll<FileAssignment>(x => x.CourseId == searchFileAssignmentRequest.CourseId)));
            //}
            if (searchFileAssignmentRequest.StudentId.HasValue)
            {
                return Ok(new ApiOkResponse(assignmentRepository
                    .GetAll<FileAssignment>(x => x.StudentId == searchFileAssignmentRequest.StudentId)));
            }
            if (searchFileAssignmentRequest.AssignmentId.HasValue)
            {
                return Ok(new ApiOkResponse(assignmentRepository
                    .GetAll<FileAssignment>(x => x.AssignmentId == searchFileAssignmentRequest.AssignmentId)));
            }
            return Ok(new ApiOkResponse(assignmentRepository.GetAll<FileAssignment>(x => x.StudentName != "")));
        }

        [HttpPost]
        public ActionResult<ApiOkResponse<Assignment>> Add(FileAssignment fileAssignment)
        {
            var assignment = assignmentRepository.GetOne<Assignment>(x => x.Id == fileAssignment.AssignmentId);
            if (assignment == null)
            {
                return BadRequest(new ApiBadRequestResponse($"Assignment {fileAssignment.AssignmentId} not found"));
            }

            //fileAssignment.CourseId = assignment.CourseId;
            fileAssignment.Score = null;
            fileAssignment.Version = DateTime.Now.DateTimeToInt();
            fileAssignment.AddedAtUtc = DateTime.UtcNow;
            fileAssignment.Status = 0;
            assignmentRepository.AddOne<FileAssignment>(fileAssignment);
            return Ok(new ApiOkResponse(fileAssignment));
        }

        [Route("updatescore")]
        [HttpPost]
        public ActionResult<ApiOkResponse<FileAssignment>> UpdateScore(UpdateScoreRequest updateScoreRequest)
        {
            var fileAssignment = fileAssignmentRepository.GetById<FileAssignment>(updateScoreRequest.Id);

            fileAssignment.Score = updateScoreRequest.Score;
            fileAssignment.ReviewDate = DateTime.Now;
            fileAssignment.Status = updateScoreRequest.Status;

            fileAssignmentRepository.UpdateOne(fileAssignment);
            return Ok(new ApiOkResponse(fileAssignment));
        }

        [Consumes("multipart/form-data")]
        [HttpPost("UploadFiles")]
        public async Task<ActionResult<ApiOkResponse<string>>> Post(IFormFile file)
        {
            if (file == null)
            {
                return BadRequest(new ApiBadRequestResponse($"File not found"));
            }
            List<string> listString = new List<string>();
            //long size = files.Sum(f => f.Length);

            // full path to file in temp location

            //foreach (var formFile in files)
            //{
            using (var streamFile = file.OpenReadStream())
            {
                var task = new FirebaseStorage("hackathon-a74c5.appspot.com")

                .Child("FileAssignment")
                .Child(file.FileName)
                .PutAsync(streamFile);

                var downloadUrl = await task;

                listString.Add(downloadUrl);
                return Ok(new ApiOkResponse(listString));
            }

            //}

            // process uploaded files
            // Don't rely on or trust the FileName property without validation.
        }
    }
}