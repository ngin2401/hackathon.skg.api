﻿using Hackathon.SKG.Api.Model;
using Hackathon.SKG.Api.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Hackathon.SKG.Api.Controllers
{
    public class HomeController : Controller
    {


        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("error/{code}")]
        public IActionResult Error(int code)
        {
            return StatusCode(code, new ApiResponse(code));
        }
    }
}