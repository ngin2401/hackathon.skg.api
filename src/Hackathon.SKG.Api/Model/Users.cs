﻿using MongoDbGenericRepository.Models;
using System;
using System.Collections.Generic;

namespace Hackathon.SKG.Api.Model
{
    public class Users : Document
    {
        //[BsonId]
        //// standard BSonId generated by MongoDb
        //public ObjectId Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public string Role { get; set; }

        public List<string> NotiToken { get; set; }
    }

    public class SearchUsersRequest
    {
        public string Name { get; set; }
        public Guid? Id { get; set; }
    }

    public class AddTokenFromUserRequest
    {
        public string NotiToken { get; set; }
        public Guid Id { get; set; }
    }

    public class LoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

}