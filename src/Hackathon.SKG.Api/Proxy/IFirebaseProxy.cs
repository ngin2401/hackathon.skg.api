﻿using FcmSharp.Requests;
using FcmSharp.Responses;
using Hackathon.SKG.Api.Dto;

namespace Hackathon.SKG.Api.Proxy
{
    public interface IFirebaseProxy
    {
        FcmMessageResponse SendNotification(FcmMessage fcmMessage);
        FcmMessageResponse SendNotification(SendNotificationRequest sendNotificationRequest);
    }
}