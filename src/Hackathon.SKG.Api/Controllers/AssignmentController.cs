﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hackathon.SKG.Api.Model;
using Hackathon.SKG.Api.Repository;
using Hackathon.SKG.Api.Utils;
using Microsoft.AspNetCore.Mvc;

namespace Hackathon.SKG.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssignmentController : ControllerBase
    {
        private IAssignmentRepository assignmentRepository;

        private IFileAssignmentRepository fileAssignmentRepository;


        public AssignmentController(IAssignmentRepository assignmentRepository
            , IFileAssignmentRepository fileAssignmentRepository
            )
        {
            this.assignmentRepository = assignmentRepository;
            this.fileAssignmentRepository = fileAssignmentRepository;
        }

        /// <summary>
        /// để null get all, truyền id get id nếu không lấy name
        /// </summary>
        /// <param name="searchAssignmentRequest">để null get all, truyền id get id nếu không lấy name</param>
        /// <returns></returns>
        // GET api/values
        [HttpGet]
        public ActionResult<ApiOkResponse<List<Assignment>>> Get([FromQuery] SearchAssignmentRequest searchAssignmentRequest = null)
        {
            if (searchAssignmentRequest == null
                || searchAssignmentRequest.Id.HasValue == false && (string.IsNullOrEmpty(searchAssignmentRequest.Name)))
            {
                return Ok(new ApiOkResponse(assignmentRepository.GetAll<Assignment>(x => x.Name != "")));
            }
            if (searchAssignmentRequest.Id.HasValue)
            {
                return Ok(new ApiOkResponse(assignmentRepository.GetById<Assignment>(searchAssignmentRequest.Id.Value)));
            }
            //if (searchAssignmentRequest.CourseId.HasValue)
            //{
            //    return Ok(new ApiOkResponse(assignmentRepository.GetAll<Assignment>(x=>x.CourseId == searchAssignmentRequest.CourseId)));
            //}
            return Ok(new ApiOkResponse(assignmentRepository.GetAll<Assignment>(x => x.Name == searchAssignmentRequest.Name)));
        }

        

        [HttpPost]
        public ActionResult<ApiOkResponse<Assignment>> Add(Assignment assignment)
        {
            assignment.Version = DateTime.Now.DateTimeToInt();
            assignment.AddedAtUtc = DateTime.UtcNow;
            assignmentRepository.AddOne<Assignment>(assignment);
            return Ok(new ApiOkResponse(assignment));
        }

        [HttpPut]
        public ActionResult<ApiOkResponse<Assignment>> Update(Assignment assignment)
        {
            assignment.Version = DateTime.Now.DateTimeToInt();
            //assignment.AddedAtUtc = DateTime.UtcNow;
            assignmentRepository.UpdateOne<Assignment>(assignment);
            return Ok(new ApiOkResponse(assignment));
        }

        [Route("GetAssignmentsByUserId")]
        [HttpGet]
        public ActionResult<ApiOkResponse<List<Assignment>>> GetAssignmentsByUserId(Guid userId)
        {
            var result = assignmentRepository.GetAssignmentsByUserId(userId);
            return Ok(new ApiOkResponse(result));
        }
    }
}