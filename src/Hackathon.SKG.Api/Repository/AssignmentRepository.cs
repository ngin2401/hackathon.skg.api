﻿using Hackathon.SKG.Api.Model;
using Hackathon.SKG.Api.Model.Settings;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDbGenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon.SKG.Api.Repository
{
    public class AssignmentRepository : BaseMongoRepository, IAssignmentRepository
    {
        public AssignmentRepository(IOptions<MongoConnection> settingMongoConnection) 
            : base(settingMongoConnection.Value.ConnectionString, settingMongoConnection.Value.Database)
        {

        }
       
        public List<Assignment> GetAssignmentsByUserId(Guid userId)
        {
            var result = GetAll<Assignment>(x => x.ListStudent.Contains(userId));
            return result;
        }
    }
}
