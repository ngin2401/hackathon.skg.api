﻿using MongoDbGenericRepository.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hackathon.SKG.Api.Model
{
    public class FileAssignment : Document
    {
        [Required]
        public Guid AssignmentId { get; set; }

        //public Guid CourseId { get; set; }

        [Required]
        public Guid StudentId { get; set; }

        [Required]
        public string StudentName { get; set; }

        [Required]

        public List<string> FileUrls { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? Score { get; set; }
        public DateTime? ReviewDate { get; set; }

        public int Status { get; set; }
    }

    public class SearchFileAssignmentRequest
    {
        public Guid? Id { get; set; }
        public Guid? AssignmentId { get; set; }
        //public Guid? CourseId { get; set; }
        public Guid? StudentId { get; set; }

        public int? Status { get; set; }
    }
    public class UpdateScoreRequest
    {
        public Guid Id { get; set; }
        public int Score { get; set; }

        public int Status { get; set; }
    }
}