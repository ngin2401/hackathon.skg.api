﻿using FcmSharp.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon.SKG.Api.Dto
{
    public class SendNotificationRequest
    {
        public Guid Id { get; set; }
        public string ClientToken { get; set; }
        public Notification Notification { get; set; }
    }
    public class SendAllStudentByAssignment
    {
        public Guid AssignmentId { get; set; }

    }
}
