﻿using Hackathon.SKG.Api.Model;
using Hackathon.SKG.Api.Repository;
using Hackathon.SKG.Api.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Hackathon.SKG.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private ICoursesRepository coursesRepository;

        public CoursesController(
            ICoursesRepository coursesRepository
            )
        {
            this.coursesRepository = coursesRepository;
        }

        /// <summary>
        /// để null get all, truyền id get id nếu không lấy name
        /// </summary>
        /// <param name="searchCoursesRequest">để null get all, truyền id get id nếu không lấy name</param>
        /// <returns></returns>
        // GET api/values
        [HttpGet]
        public ActionResult<ApiOkResponse<List<Courses>>> Get([FromQuery] SearchCoursesRequest searchCoursesRequest = null)
        {
            if (searchCoursesRequest == null 
                || searchCoursesRequest.Id.HasValue == false && (string.IsNullOrEmpty(searchCoursesRequest.Name)))
            {
                return Ok(new ApiOkResponse(coursesRepository.GetAll<Courses>(x=>x.Name != "")));
            }
            if (searchCoursesRequest.Id.HasValue)
            {
                return Ok(new ApiOkResponse(coursesRepository.GetById<Courses>(searchCoursesRequest.Id.Value)));
            }
            return Ok(new ApiOkResponse(coursesRepository.GetAll<Courses>(x => x.Name == searchCoursesRequest.Name)));
        }



        [HttpPost]
        public ActionResult<ApiOkResponse<Courses>> Add(Courses courses)
        {
            courses.Version = DateTime.Now.DateTimeToInt();
            courses.AddedAtUtc = DateTime.UtcNow;
            coursesRepository.AddOne<Courses>(courses);
            return Ok(new ApiOkResponse(courses));
        }

        [HttpPut]
        public ActionResult<ApiOkResponse<Courses>> Update(Courses courses)
        {
            courses.Version = DateTime.Now.DateTimeToInt();
            //courses.AddedAtUtc = DateTime.UtcNow;
            coursesRepository.UpdateOne<Courses>(courses);
            return Ok(new ApiOkResponse(courses));
        }
    }
}