﻿using Hackathon.SKG.Api.Model;
using Hackathon.SKG.Api.Model.Settings;
using Microsoft.Extensions.Options;
using MongoDbGenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon.SKG.Api.Repository
{
    public class CoursesRepository : BaseMongoRepository, ICoursesRepository
    {
        public CoursesRepository(IOptions<MongoConnection> settingMongoConnection) 
            : base(settingMongoConnection.Value.ConnectionString, settingMongoConnection.Value.Database)
        {

        }
       
    }
}
