﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hackathon.SKG.Api.Model.Settings
{
    public class MongoConnection
    {
        public string ConnectionString;
        public string Database;
    }
}
