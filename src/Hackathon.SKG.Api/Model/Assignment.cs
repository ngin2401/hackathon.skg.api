﻿using MongoDbGenericRepository.Models;
using System;
using System.Collections.Generic;

namespace Hackathon.SKG.Api.Model
{
    public class Assignment : Document
    {
        public string Name { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }

        public Guid TeacherId { get; set; }
        //public Guid CourseId { get; set; }

        public List<Guid> ListStudent { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
    public class SearchAssignmentRequest
    {
        public string Name { get; set; }
        public Guid? Id { get; set; }

        public Guid? CourseId { get; set; }
    }


}