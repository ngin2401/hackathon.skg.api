﻿using Hackathon.SKG.Api.Model;
using Hackathon.SKG.Api.Model.Settings;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDbGenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hackathon.SKG.Api.Repository
{
    public class UsersRepository : BaseMongoRepository, IUsersRepository
    {
        public UsersRepository(IOptions<MongoConnection> settingMongoConnection)
            : base(settingMongoConnection.Value.ConnectionString, settingMongoConnection.Value.Database)
        {
        }

        public List<Users> GetAll()
        {
            return GetAll<Users>(_ => true);
        }

        public List<Users> GetByIds(List<Guid> ids)
        {
            //var collection = MongoDbContext.Database.GetCollection<Users>("Users");

            //var filterDef = new FilterDefinitionBuilder<Users>();
            //var filter = filterDef.In(x => x.Id, ids);
            //var result = collection.Find(filter).ToList();

            var query = GetAll<Users>(x=> ids.Contains(x.Id));

            return query;
        }
    }
}