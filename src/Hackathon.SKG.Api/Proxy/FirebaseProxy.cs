﻿using FcmSharp;
using FcmSharp.Requests;
using FcmSharp.Responses;
using FcmSharp.Settings;
using Hackathon.SKG.Api.Dto;
using Microsoft.Extensions.Logging;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Hackathon.SKG.Api.Proxy
{
    public class FirebaseProxy : IDisposable, IFirebaseProxy
    {
        private FcmClientSettings fcmClientSettings;
        private FcmClient fcmClient;
        private readonly ILogger<FirebaseProxy> logger;

        public FirebaseProxy(ILogger<FirebaseProxy> logger)
        {
            this.logger = logger;
            fcmClientSettings = FileBasedFcmClientSettings.CreateFromFile("hackathon-a74c5", @"FireBaseKey.json");
            fcmClient = new FcmClient(fcmClientSettings);
        }

        public FcmMessageResponse SendNotification(FcmMessage fcmMessage)
        {
            logger.LogInformation("SendNotification {0}", fcmMessage.Dump());
            // Finally send the Message and wait for the Result:
            CancellationTokenSource cts = new CancellationTokenSource();

            // Send the Message and wait synchronously:
            var result = fcmClient.SendAsync(fcmMessage, cts.Token).GetAwaiter().GetResult();
            logger.LogDebug("SendNotification result {0}", result.Dump());
            return result;
        }

        public FcmMessageResponse SendNotification(SendNotificationRequest sendNotificationRequest)
        {
            var data = new Dictionary<string, string>()
                {
                    {"A", "B"},
                    {"C", "D"}
                };
            FcmMessage fcmMessage = new FcmMessage()
            {
                ValidateOnly = false,
                Message = new Message()
                {
                    Notification = sendNotificationRequest.Notification,
                    Token = sendNotificationRequest.ClientToken,
                    //Data = data
                }
            };
            var result = SendNotification(fcmMessage);
            return result;
        }

        public void Dispose()
        {
            fcmClient.Dispose();
        }
    }
}