﻿using Hackathon.SKG.Api.Model;
using Hackathon.SKG.Api.Repository;
using Hackathon.SKG.Api.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hackathon.SKG.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUsersRepository usersRepository;

        public UsersController(
            IUsersRepository usersRepository
            )
        {
            this.usersRepository = usersRepository;
        }

        /// <summary>
        /// để null get all, truyền id get id nếu không lấy name
        /// </summary>
        /// <param name="searchUsersRequest">để null get all, truyền id get id nếu không lấy name</param>
        /// <returns></returns>
        // GET api/values
        [HttpGet]
        public ActionResult<ApiOkResponse<List<Users>>> Get([FromQuery] SearchUsersRequest searchUsersRequest = null)
        {
            if (searchUsersRequest == null 
                || searchUsersRequest.Id.HasValue == false && (string.IsNullOrEmpty(searchUsersRequest.Name)))
            {
                return Ok(new ApiOkResponse(usersRepository.GetAll()));
            }
            if (searchUsersRequest.Id.HasValue)
            {
                return Ok(new ApiOkResponse(usersRepository.GetById<Users>(searchUsersRequest.Id.Value)));
            }
            return Ok(new ApiOkResponse(usersRepository.GetAll<Users>(x => x.Name == searchUsersRequest.Name)));
        }



        [HttpPost]
        public ActionResult<ApiOkResponse<Users>> Add(Users users)
        {
            users.Version = DateTime.Now.DateTimeToInt();
            users.AddedAtUtc = DateTime.UtcNow;
            usersRepository.AddOne<Users>(users);
            return Ok(new ApiOkResponse(users));
        }

        [HttpPut]
        public ActionResult<ApiOkResponse<Users>> Update(Users users)
        {
            users.Version = DateTime.Now.DateTimeToInt();
            //users.AddedAtUtc = DateTime.UtcNow;
            usersRepository.UpdateOne<Users>(users);
            return Ok(new ApiOkResponse(users));
        }

        [Route("AddNotiTokenFromUser")]
        [HttpPost]
        public ActionResult<ApiOkResponse<Users>> AddNotiTokenFromUser([FromBody]AddTokenFromUserRequest addTokenFromUserRequest)
        {
            var user = usersRepository.GetById<Users>(addTokenFromUserRequest.Id);
            if (user == null)
            {
                return BadRequest(new ApiBadRequestResponse(string.Format("Không tìm thấy user {0}", addTokenFromUserRequest.Id)));
            }
            if (user.NotiToken == null)
            {
                user.NotiToken = new List<string>();
            }
            if (user.NotiToken.Any(x => x == addTokenFromUserRequest.NotiToken) == false)
            {
                user.NotiToken.Add(addTokenFromUserRequest.NotiToken);
                usersRepository.UpdateOne<Users>(user);
            }
          
            return Ok(new ApiOkResponse(user));
        }

        [HttpPost]
        [Route("login")]
        public ActionResult<ApiOkResponse<Users>> Login(LoginRequest loginRequest)
        {
            return Ok(new ApiOkResponse(usersRepository.GetOne<Users>(
                x => x.Username == loginRequest.Username
            && x.Password == loginRequest.Password)));
        }


        [Route("getbyids")]
        [HttpPost]
        public ActionResult<ApiOkResponse<List<Users>>> GetByIds([FromBody] Guid[] ids)
        {
            if (ids.Length <= 0)
            {
                return BadRequest(new ApiBadRequestResponse(string.Format("Vui lòng nhập id")));
            }
            var result = usersRepository.GetByIds(ids.ToList());
            return Ok(new ApiOkResponse(result));
        }

    }
}